#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int creados=0;
int status;
int sig2=0;
pid_t var; 
pid_t *hijos_pid = NULL;

void handler(int);
void handler2(int);
void terminar(int);


int main(void){
 signal(SIGUSR1,handler);
 signal(SIGUSR2,handler2);
 signal(SIGTERM,terminar);
 printf("Espero recibir una señal para PID %d\n", getpid());

 do
 {
  pause();

  switch(sig2){
  case 10:
    if ((var = fork()) == 0) {
        do{
        printf("\tsoy el hijo mi PID es %d y el de mi padre %d\n", getpid(), getppid());
        sleep(5); // Simulación de trabajo en el hijo.
        }while(1);
        
    } else {
        // Este es el proceso padre.
        creados=creados+1;
        hijos_pid = realloc(hijos_pid, creados * sizeof(pid_t));
        hijos_pid[creados - 1] = var;
    }
  break;

  case 12:
   if ((var=fork())==0){
   printf( "\tsoy el hijo mi PID es %d \n", getpid());
   execlp("ls","ls", NULL);
   }
  break;

  case 15:
    printf("los proceso creados son %d\n",creados);
    for (int i = 0; i < creados; i++) {
        printf( "\tSe termina el proceso hijo con PID %d \n", hijos_pid[i]);
        kill(hijos_pid[i], SIGKILL);
        waitpid(hijos_pid[i],&status,0);
        sleep(1);
    }
    return 0; //termino programa
  break;
  
 }

 }while (1);
}

void handler(int sig){
 printf("Se recibio la señal %d\n",sig);
 sig2=sig;
}

void handler2(int sig){
 printf("Se recibio la señal %d\n",sig);
 sig2=sig;
}

void terminar(int sig){
 printf("Se recibio la señal %d\n",sig);
 sig2=sig;
}

