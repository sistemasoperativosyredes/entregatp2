# README - Gestión de Señales en C

Este programa en C demuestra la gestión de señales utilizando las señales **SIGUSR1**, **SIGUSR2** y **SIGTERM**. A continuación, se presenta una descripción de cómo se utilizan estas señales en el código:

## Inicialización de Variables y Funciones Manejadoras de Señales

Comenzamos el código inicializando las variables a utilizar y definiendo las funciones manejadoras de señal:

- **creados**: Contador de procesos hijos creados.
- **status**: Variable para almacenar el estado de los procesos hijos.
- **sig2**: Variable que almacena el número de la señal recibida.
- **var**: Almacena el PID de los procesos hijos.
- **hijos_pid**: Arreglo dinámico que guarda los PID de los procesos hijos.

Funciones manejadoras de señales:
- `handler`: Maneja la señal **SIGUSR1**.
- `handler2`: Maneja la señal **SIGUSR2**.
- `terminar`: Maneja la señal **SIGTERM**.

## Uso de `signal` para Asociar Señales a Funciones Manejadoras

En el programa principal, utilizamos el comando `signal` para indicar qué tipo de señal será manejada por cada función manejadora de señal.

## Recepción de Señales y Ejecución de Acciones

Esperamos a que se reciba una señal: **SIGUSR1**, **SIGUSR2** o **SIGTERM**, utilizando `pause`. Una vez recibida alguna de estas señales, establecemos el valor de la variable `sig2`, que determina qué acción se ejecutará a continuación.

### **SIGUSR1** (sig2=10)

- Se crea un hijo.
- Se imprime el PID del hijo y el PID del padre.
- Se simula un trabajo en el hijo con un retardo de 5 segundos.
- El padre registra la cantidad de hijos creados y almacena los PID de los hijos en `hijos_pid`.

### **SIGUSR2** (sig2=12)

- Se crea un hijo.
- Se imprime el PID del hijo.
- Se ejecuta el comando `ls` en el hijo utilizando `execlp`.
- El hijo creado termina después de ejecutar `ls`.

### **SIGTERM** (sig2=15)

- El padre muestra la cantidad de procesos hijos creados.
- El padre comienza a terminar a sus hijos uno por uno.
  - Envia la señal **SIGKILL** a uno de los hijos utilizando `kill`.
  - Espera a que se liberen los recursos del hijo con `waitpid`.
  - Espera un segundo antes de continuar con el siguiente hijo.

Una vez que todos los hijos han sido terminados, el programa finaliza.
